# vlc-bridge-ustvnow

watch USTVNOW live stream in VLC

### using

`$ docker run -d -e 'USTVNOW_USER=user@email.com' -e 'USTVNOW_PASS=secret' -p 7777:7777 --name vlc-bridge-ustvnow registry.gitlab.com/miibeez/vlc-bridge-ustvnow`

`$ vlc http://localhost:7777/ustvnow/playlist.m3u`

### require

military member for USA
