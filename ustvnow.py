import json
import os
import sys
import time
from threading import Lock
import requests
import uuid
import base64

from Cryptodome.Cipher import AES
from Cryptodome.Hash import SHA256
from Cryptodome.Util.Padding import pad, unpad

rkey = "".join(reversed("80035ad42d7d-bb08-7a14-f726-78403b29")).encode("utf8")
riv = "".join(reversed("3157b5680927cc4a")).encode("utf8")
fkey = SHA256.new(rkey).hexdigest()[:32].encode("utf8")

if os.environ.get("USTVNOW_USER") is None or os.environ.get("USTVNOW_PASS") is None:
    sys.stderr.write("USTVNOW_USER and USTVNOW_PASS need set\n")
    sys.exit(1)

class Client:
    def __init__(self):
        self.user = os.environ["USTVNOW_USER"]
        self.passwd = os.environ["USTVNOW_PASS"]

        self.device = None
        self.loggedIn = False
        self.sessionID = ""
        self.sessionAt = 0
        self.mutex = Lock()

        self.load_device()

    def channels(self):
        with self.mutex:
            resp, error = self.api("tv/guide?page=0&pagesize=150&skip_tabs=1", None)
            if error:
                return None, error

            stations = []
            for ch in resp["response"]["data"]:
                for p in ch["programs"]:
                    if p["target"]["pageAttributes"].get("isLive") == "true":
                        station_id = p["target"]["path"].replace("channel/live/", "", 1).replace("/", "-")
                        station_name = ch["channel"]["display"]["title"]
                        station = {"id": station_id, "name": station_name}
                        stations.append(station)
                        break
            return stations, None

    def watch(self, id):
        with self.mutex:
            if not self.loggedIn:
                error = self.login()
                if error:
                    return None, error

            path = "channel/live/" + id.replace("-", "/")
            resp, error = self.enc_api("send", {
                "path": path,
            }, {
                "request": "page/stream",
            })
            if error:
                return None, error

            if not resp["status"]:
                message = resp["error"]["message"]
                code = resp["error"]["code"]
                return None, f"Stream failure: {message} ({code})"

            for s in resp["response"]["streams"]:
                url = s["url"]
                return url, None

            return None, "missing stream"

    def load_device(self):
        with self.mutex:
            try:
                with open("ustvnow-device.json", "r") as f:
                    self.device = json.load(f)
            except FileNotFoundError:
                self.device = str(uuid.uuid4())
                with open("ustvnow-device.json", "w") as f:
                    json.dump(self.device, f)

    def token(self):
        if self.sessionID != "" and (time.time() - self.sessionAt) < 4 * 60 * 60:
            return self.sessionID, None
        self.loggedIn = False

        params = {
            "box_id": self.device,
            "tenant_code": "ustvnow",
            "product": "ustvnow",
            "device_id": "5",
            "device_sub_type": "",
            "display_lang_code": "ENG",
            "timezone": "UTC",
        }
        response = requests.get("https://teleupapi.revlet.net/service/api/v1/get/token", params=params).json()

        if not response["status"]:
            error_details = response["error"]["details"]
            error_detail = response["error"]["detail"]
            return None, f"Token failure: {error_details} ({error_detail})"

        self.sessionID = response["response"]["sessionId"]
        self.sessionAt = time.time()
        return response["response"]["sessionId"], None

    def api(self, cmd, data):
        token, error = self.token()
        if error:
            return None, error

        headers = {
            "box-id": self.device,
            "session-id": token,
            "tenant-code": "ustvnow",
        }
        url = f"https://teleupapi.revlet.net/service/api/v1/{cmd}"
        if data:
            response = requests.post(url, json=data, headers=headers)
        else:
            response = requests.get(url, headers=headers)
        return response.json(), None

    def enc_api(self, cmd, data, metadata):
        jdata = json.dumps(data)
        jmdata = json.dumps(metadata)
        resp, error = self.api(cmd, {
            "data": enc(jdata),
            "metadata": enc(jmdata),
        })
        if error:
            return None, error

        jdata = dec(resp["data"])
        return json.loads(jdata), None

    def login(self):
        resp, error = self.enc_api("send", {
            "login_id": self.user,
            "login_key": self.passwd,
            "login_mode": "1",
            "manufacturer": "123",
        }, {
            "request": "signin",
        })
        if error:
            return error

        if not resp["status"]:
            message = resp["error"]["message"]
            code = resp["error"]["code"]
            return f"Login failure: {message} ({code})"
        self.loggedIn = True
        return None

def enc(data):
    cipher = AES.new(fkey, AES.MODE_CBC, riv)
    encrypted = cipher.encrypt(pad(data.encode('utf-8'), 16, "pkcs7"))
    return base64.b64encode(encrypted).decode('utf-8')

def dec(data):
    cipher = AES.new(fkey, AES.MODE_CBC, riv)
    decrypted = cipher.decrypt(base64.b64decode(data))
    if decrypted:
        return unpad(decrypted, 16, "pkcs7")
    else:
        return decrypted